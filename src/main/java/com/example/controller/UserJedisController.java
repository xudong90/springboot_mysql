package com.example.controller;

import com.example.common.ResponseInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RequestMapping("/jedis")
@RestController
@Api(value = "UserJedisController" ,description = "使用jedisPool连接redis")
public class UserJedisController {
    @Autowired
    private JedisPool jedisPool;

    @ApiOperation(value = "使用jedisPool的jedis对象")
    @RequestMapping(value = "/getData",method = {RequestMethod.POST})
    public ResponseInfo<Object> userJedis(){
        Jedis jedis = jedisPool.getResource();
        ResponseInfo responseInfo = new ResponseInfo();
        String str = jedis.set("jedis1","ddddddddddddd");
        responseInfo.setData(str);
        jedis.close();
        return responseInfo;
    }
    @ApiOperation(value = "jedis的方法")
    @RequestMapping(value = "/testJedis",method = {RequestMethod.POST})
    public Map getKeysList(){
        Jedis jedis = jedisPool.getResource();
        Map<String,Object> map = new HashMap<>();
        Set<String> keySet = jedis.keys("*"); //列出所有的key
        map.put("keys",keySet);

        return map;
    }
}
