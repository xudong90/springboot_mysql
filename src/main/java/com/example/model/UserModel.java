package com.example.model;

import java.io.Serializable;

public class UserModel implements Serializable {
    private Integer id;
    private String userName;
    private String email;
    private Integer gender; // 1 男  0 女

    public UserModel() {
    }

    public UserModel(Integer id, String userName, String email, Integer gender) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.gender = gender;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }
    @Override
    public String toString(){
        return "UserModel = [id="+this.id+",username="+this.userName+",email="+this.email+",gender="+this.gender+"]";
    }
}
