package com.example.config.datasource;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 2. 创建线程安全的类，作为databaseType容器，放master,slave1,slave2
 */
public class DatabaseContextHolder {
    private static final ThreadLocal<DatabaseType> contextHolder = new ThreadLocal<>();
    private static final AtomicInteger counter = new AtomicInteger(-1);

    public static void set(DatabaseType databaseType){
        contextHolder.set(databaseType);
    }
    public static DatabaseType get(){
        return contextHolder.get();
    }
    public static void setDataBaseMaster() {
        set(DatabaseType.master);
        System.out.println("=====================> 切换到master");
    }
    public static void setDataBaseSlave() {
        //  轮询
        int index = counter.getAndIncrement() % 2;
        if (counter.get() > 9999) {
            counter.set(-1);
        }
        if (index == 0) {
            set(DatabaseType.slave1);
            System.out.println("==================> 切换到slave1");
        }else {
            set(DatabaseType.slave2);
            System.out.println("==================> 切换到slave2");
        }
    }
}
