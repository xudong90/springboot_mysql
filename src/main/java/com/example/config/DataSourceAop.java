package com.example.config;

import com.example.config.datasource.DatabaseContextHolder;
import com.example.config.datasource.DatabaseType;
import com.example.dao.annotation.MasterDataSource;
import io.swagger.annotations.Extension;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DataSourceAop {

    @Before("@annotation(com.example.dao.annotation.ReadDataSouce)")
    public void readPoint(){
        DatabaseContextHolder.setDataBaseSlave();
    }
    @Before("@annotation(com.example.dao.annotation.MasterDataSource)")
    public void writePoint(){
        DatabaseContextHolder.setDataBaseMaster();
    }
}
