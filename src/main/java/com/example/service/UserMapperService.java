package com.example.service;

import com.example.dao.UserMapper;
import com.example.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = "userCache")
public class UserMapperService  {
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    UserMapper userMapper;
    @Cacheable(cacheNames = {"userCache"},key = "#id")
    public UserModel getUserById(Integer id) {
        return userMapper.getUserById(id);
    }

    @SuppressWarnings("SpringCacheableComponentsInspection")
    @Cacheable(cacheNames = {"userCache1"},keyGenerator = "nameKeyGenerator")
    public UserModel getUserById2(Integer id) {
        return userMapper.getUserById(id);
    }

    @CachePut(keyGenerator = "nameKeyGenerator2",condition = "#userModel.id != null")
    public UserModel insertUser(UserModel userModel) {
        userMapper.insertUser(userModel);
        int id = userMapper.getCurrentIdSeq();
        userModel.setId(id);
        return userModel;
    }


    public void updateUserById(UserModel userModel) {
        userMapper.updateUserById(userModel);
    }

    @CacheEvict(value = "userCache1",key = "#id")
    public void deleteUserById(Integer id) {
        userMapper.deleteUserById(id);
    }
    @Cacheable(key = "#count")
    public Integer getUserInfoCount(String count){
        return userMapper.getCount();
    }

}
