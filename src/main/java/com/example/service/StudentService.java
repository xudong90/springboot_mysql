package com.example.service;


import com.example.config.datasource.DatabaseContextHolder;
import com.example.config.datasource.DatabaseType;
import com.example.dao.StudentMapper;
import com.example.dao.annotation.MasterDataSource;
import com.example.dao.annotation.ReadDataSouce;
import com.example.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2019/3/15.
 */
@Service
@EnableTransactionManagement
public class StudentService implements StudentMapper {
    @Autowired
    private StudentMapper studentMapper;
    @Override
    @ReadDataSouce
    public int getCount() {
        return studentMapper.getCount();
    }

    @Override
    @ReadDataSouce
    public List<Student> getStuList() {
        return studentMapper.getStuList();
    }

    @Override
    @MasterDataSource
    public int insert(Student student) {
        return studentMapper.insert(student);
    }

    @Override
    @ReadDataSouce
    public List<Student> getStuByAge(int age) {
        return studentMapper.getStuByAge(age);
    }

    @Override
    @ReadDataSouce
    public List<Student> getStuByIdScope(Map<String,Object> map) {
        return studentMapper.getStuByIdScope(map);
    }
}
